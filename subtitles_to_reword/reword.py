from typing import List, Dict

from collections import OrderedDict
import codecs
import os
from statistics import mode
import random

from . import netflix_vtt
from . import google_translate

"""
This set of functions is used to turn subtitles downloaded from Netflix
into a dictionary that can be fed into the ReWord android language learning app.
Userscript to download Netflix subtitles:
https://greasyfork.org/en/scripts/26654-netflix-subtitle-downloader
ReWord on Google Play:
https://play.google.com/store/apps/details?id=ru.poas.englishwords&hl=en&gl=US

How to use:
The best way I could find to get words translated is Google Translate,
which seems to make obsolete any automated way of using it pretty quickly
whenever a new method pops up.
For this reason, using these functions requires some manual copy pasting into
and out of Google Translate.
"""

class ReWordEntry:
    def __init__(self):
        # Forms of the same word can have different capitalization. 
        # Used to find the most common capitalization.
        self.native: str = ""
        self.foreign_forms: List(str) = []
        self.examples: List(Tuple) = []

    @property
    def foreign(self):
        """Return the most common of all foreign forms."""
        try:
            # Pick most common
            return mode(self.foreign_forms)
        except:
            # Pick at random
            return self.foreign_forms[0]

    def to_reword(self, max_examples=5, randomize_examples=True) -> str:
        """Return the line for the .csv file that will be imported into ReWord for this word."""
        word = f'"{self.foreign}";"{self.native}"'
        examples = []
        if randomize_examples:
            random.shuffle(self.examples)
        for i, example in enumerate(self.examples):
            if len(example[0]) > 500 or len(example[1]) > 500:
                continue
            if len(examples) > max_examples and max_examples > 0:
                break
            ex_de = example[0].replace('"', "'")
            ex_en = example[1].replace('"', "'")
            examples.append(f'"{ex_en}";"{ex_de}"')
        parts = [word]
        parts.extend(examples)
        line = ";".join(parts) + "\n"
        return line

def reword_map_phrases(native_filepath: str, foreign_filepath: str, out_filepath: str):
    """Write a dictionary mapping to ReWord CSV format without any examples.
    The format:
    "This is a word";"Das ist ein Wort";"Phonetics (can leave empty)";"Das ist ein beispiel";"This is an example";"Ein anderes beispiel";"Another example!"
    "New line means new entry";"Ein anderes wort"
    """
    native_subs = netflix_vtt.vtt_load_file(native_filepath)
    foreign_subs = netflix_vtt.vtt_load_file(foreign_filepath)

    mapped = netflix_vtt.map_subtitles(native_subs, foreign_subs)

    merged = netflix_vtt.merge_duplicates(mapped)

    f = codecs.open(out_filepath, "w", "utf-8")
    for native, foreign in merged.items():
        # Turn newlines into escaped newlines TODO: Newline characters aren't actually supported by ReWord afaict.
        native = native.text.replace("\n", "\\n")
        foreign = foreign.text.replace("\n", "\\n")
        f.write(f'"{native}";"{foreign}"\n')
    f.close()

def reword_create_entries(phrases: Dict[str, str], word_dict: Dict[str, str]) -> List[ReWordEntry]:
    """Go through every phrase and create a ReWordEntry for each unique word, 
    accumulating their example phrases and various capitalizations."""
    
    reword_entries = OrderedDict()

    whitelist = set("-'abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ äÄüÜöÖß")
    for native_phrase in phrases.keys():
        foreign_phrase = phrases[native_phrase]
        foreign_words = ''.join(filter(whitelist.__contains__, foreign_phrase)).split(" ")

        for foreign_word in foreign_words:
            if foreign_word.startswith("-"):
                foreign_word = foreign_word[1:]
            if foreign_word.isupper():
                foreign_word = foreign_word.title()

            lower = foreign_word.lower()
            if lower not in word_dict:
                continue
            if lower not in reword_entries:
                reword_entries[lower] = ReWordEntry()

            reword_entries[lower].foreign_forms.append(foreign_word)

            reword_entries[lower].native = word_dict[lower]
            reword_entries[lower].examples.append((native_phrase, foreign_phrase))

    # Sort entries such that the ones that were in the most phrases are at the start of the list.
    reword_entries = list(reversed(sorted(reword_entries.values(), key=lambda entry: len(entry.examples))))

    return reword_entries

def reword_merge_duplicates(reword_entries: List[ReWordEntry]):
    """Combine entries with the same native meaning."""

    reword_native = {}
    for entry in reword_entries[:]:
        if entry.native.lower() not in reword_native:
            reword_native[entry.native.lower()] = entry
        else:
            existing = reword_native[entry.native.lower()]
            # print(f"Merging duplicate: {existing.foreign} = {entry.foreign} \n({existing.native}={entry.native})\n")
            existing.foreign_forms.extend(entry.foreign_forms)
            existing.examples.extend(entry.examples)
            reword_entries.remove(entry)

def phrases_to_reword(
        phrases: Dict[str, str]
        ,GT_foreign_folder: str     # Folder containing output of GT_generate_word_lists()
        ,GT_native_folder: str      # Folder created manually by copy-pasting GT_foreign_folder. Open each file, paste its contents into google translate, replace contents with the result.
        ,output_filepath: str        # File to write ReWord database
        ,start_count = 0
        ,end_count = 1000
        ):
    """Write the ReWord database."""
    word_map = google_translate.GT_load_dictionary(
        GT_native_folder, 
        GT_foreign_folder
    )
    reword_entries = reword_create_entries(
        phrases,
        word_map
    )

    reword_merge_duplicates(reword_entries)

    f = codecs.open(output_filepath, "w", "utf-8")
    for entry in reword_entries[start_count:end_count]:
        f.write(entry.to_reword())
    f.close()
