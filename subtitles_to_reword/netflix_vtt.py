from webvtt import Caption
from typing import List, Dict

import os
import webvtt   # pip install webvtt-py
import re
from collections import OrderedDict

from . import utils
from . import google_translate

def vtt_load_file(filepath: str, sanitize=True) -> List[Caption]:
    ret = []
    for caption in webvtt.read(filepath):
        ret.append(caption)

    if sanitize:
        sanitize_netflix_subtitles(ret)
    return ret


def load_folder_of_subtitles(folder_path: str) -> List[List[Caption]]:
    """Return list of list of subtitles in a folder."""
    all_subs = []
    for entry in os.scandir(folder_path):
        if entry.path.endswith(".vtt") and entry.is_file():
            subs = vtt_load_file(entry.path)
            all_subs.append(subs)
    return all_subs


def sanitize_netflix_subtitles(captions: List[Caption]):
    """Apply some rules to clean up .vtt Captions."""
    # This was so far written specifically for subtitles downloaded from 
    # Netflix's Hilda, other sources might need different rules.
    in_brackets = re.compile("\[.*\]")
    start_whitespace = re.compile("^\s+")
    end_whitespace = re.compile("[ \f\t\v]$")

    previous = captions[0]
    for caption in captions[:]:
        # Replace new-lines with space
        orig_text = caption.text
        # TODO: Could try removing this and the next to see if ReWord supports newline characters within its quotation marks, since it does not support \n.
        caption.text = caption.text.replace("\n", " ")
        caption.text = caption.text.replace("\n-", "")
        caption.text = caption.text.replace("-\n", "")
        caption.text = caption.text.replace("--", "-")

        # Remove audio descriptions, whitespace
        for regex in [in_brackets, start_whitespace, end_whitespace]:
            caption.text = re.sub(regex, '', caption.text)

        caption.text = caption.text.replace("- ", "-")
        caption.text = caption.text.replace("-\n", "")

        if caption.text in ["", "-"]:
            # print("Removing: " + orig_text)
            captions.remove(caption)
            continue

        # If this line starts with a lowercase or ., join it into the previous one.
        if caption.text[0].islower() or caption.text[0] == ".":
            previous.text += " " + caption.text
            # print("Merging: " + orig_text)
            captions.remove(caption)
            continue

        previous = caption


def find_nearest(captions: List[Caption], cap: Caption):
    # This could not be done with a kdtree because to make the best guess,
    # we have to make some assumptions that are very specific to the way
    # subtitles made by different people based on each other's work can
    # be different or similar in their start/end timings.

    start = cap.start_in_seconds
    end = cap.end_in_seconds

    # Find two nearest timings
    last_lower = -1
    first_higher = -1
    for caption in captions:
        # If either of the timings match perfectly, consider it a match.
        # This happens when one of the subtitle authors worked off of the other,
        # and can give us a lot more precision in matching our subs.
        if caption.start_in_seconds == start or caption.end_in_seconds == end:
            return caption

        if caption.start_in_seconds < start:
            last_lower = caption
            continue
        first_higher = caption
        break

    if last_lower == -1:
        last_lower = captions[0]
    if first_higher == -1:
        first_higher = captions[-1]

    # Check which one is closer
    if (start - last_lower.start_in_seconds) < (first_higher.start_in_seconds - start):
        return last_lower

    return first_higher


def map_subtitles(native_captions: List[Caption], foreign_captions: List[Caption]) -> Dict[Caption, Caption]:
    mapping = OrderedDict()
    for native in native_captions:
        mapping[native] = find_nearest(foreign_captions, native)
    return mapping


def merge_duplicates(mapping: Dict[Caption, Caption]) -> OrderedDict:
    previous_native = list(mapping.keys())[0]
    previous_foreign = list(mapping.values())[0]

    for native, foreign in list(mapping.items()):
        if foreign.text == previous_foreign.text:
            # If the native subs have some extra lines such as for characters shouting
            # join the native subs into the previous and remove this mapping
            previous_native.text += " " + native.text
            del mapping[native]
            continue
        if native.text == previous_native.text:
            # If the foreign subs have some extra lines such as for characters shouting
            # join the foreign subs into the previous and remove this mapping
            previous_foreign.text += " " + foreign.text
            del mapping[native]
            continue

        previous_native = native
        previous_foreign = foreign

    return mapping


def load_phrases_folder(VTT_native_folder: str, VTT_foreign_folder: str) -> Dict[str, str]:
    """This function expects two folders filled with an identically named set of files:
    One containing the native .VTT files, and another containing the foreign .VTT files.
    """
    
    # Load phrases
    all_phrases = OrderedDict()

    def get_vtt_files(folder): return [e.path for e in os.scandir(
        folder) if e.path.endswith(".vtt") and e.is_file()]

    native_files = get_vtt_files(VTT_native_folder)
    foreign_files = get_vtt_files(VTT_foreign_folder)
    for i, native_filepath in enumerate(native_files):
        foreign_filepath = foreign_files[i]
        native_subs = vtt_load_file(native_filepath)
        foreign_subs = vtt_load_file(foreign_filepath)

        mapped = map_subtitles(native_subs, foreign_subs)
        merged = merge_duplicates(mapped)

        # Convert Caption to str.
        for native in merged.keys():
            all_phrases[native.text] = merged[native].text
    return all_phrases

def VTT_to_google_translate(VTT_foreign_folder: str, output_path: str, ignored_words=[]):
    all_subs = load_folder_of_subtitles(VTT_foreign_folder)
    # Flatten the list with arcane double list comprehension
    all_subs = [item for sublist in all_subs for item in sublist]

    word_data = utils.phraselist_to_word_data([caption.text for caption in all_subs])

    google_translate.GT_generate_word_lists(word_data, output_path, ignored_words)
