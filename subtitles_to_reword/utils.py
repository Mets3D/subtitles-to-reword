from typing import List
from collections import OrderedDict

def phraselist_to_word_data(phrases: List[str], sort='FREQ') -> OrderedDict:
    """Return a dictionary of every unique word.
    The keys are each word fully lowercase.
    The values are lists containing the original capitalization of each occurrence of the word.
    Optionally, sort by number of occurrences.
    """
    whitelist = set(
        "-'abcdefghijklmnopqrstuvwxyz ABCDEFGHIJKLMNOPQRSTUVWXYZ äÄüÜöÖß")

    # dictionary of {match string : [word list as they appeared]}
    unique_words = OrderedDict()
    for phrase in phrases:
        phrase = phrase.replace("\n", " ")
        words = ''.join(filter(whitelist.__contains__, phrase)).split(" ")
        for word in words:
            if word.startswith("-"):
                # We want to allow "-" characters, but not at the start of the word.
                word = word[1:]
            if len(word)==1:
                continue
            if word.isupper():
                # If the word is fully capitalized, we assume it's the name of something.
                word = word.title()
            lower = word.lower()
            if "hundert" in lower:
                # Skip silly long numbers
                continue
            if lower in unique_words:
                unique_words[lower].append(word)
            else:
                unique_words[lower] = [word]

    if sort == 'FREQ':
        # Sort words by number of appearances, descending
        unique_words = OrderedDict(
            reversed(sorted(unique_words.items(), key=lambda x: len(x[1]))))
    elif sort == 'LEN':
        # Sort words by their length
        unique_words = OrderedDict(
            reversed(sorted(unique_words.items(), key=lambda x: len(x[0]))))

    return unique_words
