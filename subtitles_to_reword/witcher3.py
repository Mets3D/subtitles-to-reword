from typing import Dict

import os
import subprocess
from shutil import copyfile
import pathlib
import codecs
import re

def w3_copy_and_decode(langs=["en", "de"]):
    # Walk through the entire installation directory, finding and decoding subtitle files.
    for root, dirs, files in os.walk(WITCHER3_INSTALL_FOLDER):
        for f in files:
            if f[:2] in langs and f.endswith(".w3strings"):
                # subprocess.run(["dir"])
                full_path = os.sep.join([root, f])
                folder_path = root.replace(WITCHER3_INSTALL_FOLDER, TARGET_DIR)
                new_full_path = os.sep.join([folder_path, f])
                pathlib.Path(folder_path).mkdir(parents=True, exist_ok=True)
                copyfile(full_path, new_full_path)
                os.system(W3STRINGS_DECODER_EXE + " --decode " + new_full_path)

class Witcher3Text:
    def __init__(self, id=0, hex_key=0, str_key="", text=""):
        self.id = id
        self.hex_key = hex_key
        self.str_key = str_key
        self.text = text
    
    def __repr__(self):
        return f"{self.id}: {self.text}"

def w3_load_file(filepath: str) -> Dict[int, Witcher3Text]:
    # Load a single witcher3 csv file
    f = codecs.open(filepath, 'r', 'utf-8')
    w3texts = {}
    formatting_regex = re.compile("<.*?>")
    hotkey_regex = re.compile("<<.*?>>")
    for line in f:
        if line.startswith(";"):
            continue
        parts = line.split("|")

        old_text = parts[3]
        text = old_text.replace("<br>", "\n")
        if "[" in text:
            continue

        id = int(parts[0])
        hex_key = int(parts[1], 16)
        text = re.sub(hotkey_regex, 'X', text)
        text = re.sub(formatting_regex, '', text)

        w3texts[id] = Witcher3Text(
            id = id
            ,hex_key = hex_key
            ,str_key = parts[2]
            ,text = text
        )
    f.close()
    return w3texts

def w3_load_phrases_folder(folder: str, lang="en"):
    """Recursively walk the folder, load all .csv files starting with lang 
    into {id: Witcher3Text} dicts."""
 
    all_texts = {}
    for root, dirs, files in os.walk(folder):
        for f in files:
            if f.startswith(lang) and f.endswith(".csv"):
                all_texts.update(w3_load_file(os.sep.join([root, f])))
    return all_texts

def w3_load_mapped_phrases(folder: str) -> Dict[str, str]:
    de_phrases = w3_load_phrases_folder(
        folder = TARGET_DIR
        ,lang = foreign_lang
    )
    en_phrases = w3_load_phrases_folder(
        folder = TARGET_DIR
        ,lang = native_lang
    )

    phrase_map = {}
    for id in en_phrases.keys():
        en_phrase = en_phrases[id]
        if id not in de_phrases:
            continue
        de_phrase = de_phrases[id]
        en_text = en_phrase.text.replace("\n", "")
        de_text = de_phrase.text.replace("\n", "")
        phrase_map[en_text] = de_text
    return phrase_map
