from typing import List, Dict

from statistics import mode
import os
import codecs
from collections import OrderedDict

GOOGLE_TRANSLATE_MAX_CHARS = 5000

def GT_write_textfiles(word_list: List[str], out_filepath: str):
    """Output text files of the words in the word list with a character limit 
    in each file to match the limit of Google Translate.
    """
    if "." not in out_filepath:
        out_filepath += "out.txt"

    file_counter = 0
    filepath_parts = out_filepath.split(".")
    extension = filepath_parts[-1]

    word_idx = 0
    word = word_list[word_idx]
    while True:
        filepath_parts[-1] = f"{file_counter}.{extension}"
        filepath_full = ".".join(filepath_parts)
        f = codecs.open(filepath_full, "w", "utf-8")
        char_count = 0

        while char_count < GOOGLE_TRANSLATE_MAX_CHARS - len(word):
            f.write(f'{word}\n')
            char_count += len(word)+1

            word_idx += 1
            if word_idx >= len(word_list):
                f.close()
                return
            word = word_list[word_idx]

        file_counter += 1
        f.close()

def GT_generate_word_lists(word_data: Dict[str, List[str]], output_path: str, ignored_words=[], start_idx=0, end_idx=-1, include_count=False):
    """Generate the text files that will have to be manually churned through Google Translate.
    Google Translate has a character limit, so the files are split up into such chunks to make
    the manual process more convenient.
    """
    for key in list(word_data.keys()):
        if key in ignored_words:
            del word_data[key]



    word_list = []
    for i, key in enumerate(word_data.keys()):
        if i < start_idx: continue
        if end_idx != -1 and i > end_idx:
            break
        try:
            # Try to pick most common
            word = mode(word_data[key])
        except:
            # If fails, pick first one
            word = word_data[key][0]
        if include_count:
            word = f"{len(word_data[key])} {word}"
        word_list.append(word)

    GT_write_textfiles(word_list, output_path)

def GT_load_dictionary(native_folder: str, foreign_folder: str) -> dict:
    """Once the words have been google translated manually, load the results into
    a dictionary and return it."""
    def load_folder_into_wordlist(folder_path):
        words = []
        for entry in os.scandir(folder_path):
            if entry.path.endswith(".txt") and entry.is_file():
                f = codecs.open(entry.path, 'r', 'utf-8')
                for line in f:
                    if line == "":
                        continue
                    words.append(line.replace("\n", ""))
                f.close()
        return words

    native_words = load_folder_into_wordlist(native_folder)
    foreign_words = load_folder_into_wordlist(foreign_folder)

    assert len(native_words) == len(
        foreign_words), f"Number of words don't match: {len(native_words)} native vs {len(foreign_words)} foreign. You made a mistake in the manual Google Translate copy pasting process. Use their built-in copy to clipboard button!"

    word_map = {}
    for i, native in enumerate(native_words):
        foreign = foreign_words[i]
        if native.lower() == foreign.lower():
            continue
        
        if foreign.startswith("-"):
            foreign = foreign[1:]

        word_map[foreign.lower()] = native

    return word_map
