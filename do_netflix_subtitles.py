# Turning Netflix Subtitles into .csv files usable in the ReWord android app for language learning:
# - Download both foreign and native subtitles in .VTT format from Netflix using a browser extension.
# - Set the "root_folder" parameter below.

# - Execute this program with PHASE = 1.
# - Execute Phase 2 manually:
    # - Copy the GT_foreign_folder folder
    # - Name the copy "GT_native_folder"
    # - For each file:
    #   - Copy & Paste the contents of the file into Google Translate in a web browser
    #   - Copy & Paste the resulting translation back into the file, overwriting its previous contents.
# - Execute this program with PHASE = 3.
# - Load the resulting .csv file in Re-Word.
# - Enjoy learning some foreign words!

PHASE = 3
root_folder = "D:/Met-PC/Desktop/reWord/Hilda"

#######################################################
############# OPTIONAL PARAMETERS #####################
#######################################################

# Folders where you downloaded the foreign and native language VTT files from Netflix
# (You can do this with a browser extension, consider this Phase 0)
VTT_native_folder = root_folder + "/vtt_en/"  # Used to populate the .csv file with example sentences.
VTT_foreign_folder = root_folder + "/vtt_de/"

# Number of the most common words you want to learn
NUM_WORDS = 1000
# Words to exclude
from subtitles_to_reword import known_words
KNOWN_WORDS = known_words.known_words

# Folder where a list of words will be outputted by running this program with PHASE==1.
GT_foreign_folder = root_folder + "/google_de/"
# Folder where you operate during Phase 2, copy-pasting text into and out of Google Translate.
GT_native_folder = root_folder + "google_en/"

# Ultimate output file that can be loaded into ReWord.
final_csv_output_path = root_folder + "/output.csv"

#######################################################################
#######################################################################
#######################################################################

from subtitles_to_reword import netflix_vtt
from subtitles_to_reword import reword

# Phase 1: Read all german netflix subtitles into txt files containing
# unique words sorted by frequency, split up every 5000 characters.
# (Google Translate max characters is 5000)
if PHASE == 1:
    netflix_vtt.VTT_to_google_translate(
        VTT_foreign_folder = VTT_foreign_folder
        ,output_path = GT_foreign_folder
        ,ignored_words = KNOWN_WORDS
    )

# Phase 3: Read the subtitles again, this time not only keeping track of unique
# words but also the phrases that the words were part of. Then match the words 
# to their native meaning using the Google Translate database from Phase 2. 
# Then output everything into .csv database which can be imported in the ReWord Android app.
if PHASE == 3:
    phrases = netflix_vtt.load_phrases_folder(
        VTT_native_folder = VTT_native_folder
        ,VTT_foreign_folder = VTT_foreign_folder
    )

    reword.phrases_to_reword(
        phrases
        ,GT_foreign_folder = GT_foreign_folder
        ,GT_native_folder = GT_native_folder
        ,output_filepath = final_csv_output_path
        ,start_count = 0
        ,end_count = NUM_WORDS
    )
