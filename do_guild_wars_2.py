# Localization files can be extracted from the .dat using https://github.com/kytulendu/gw2browser.

# The file names between different languages don't match, but their order does.
# So in alphabetical order, the 125th English .csv file will have the corresponding content to the 125th German .csv file.

import os, csv
from typing import Dict

native_folder = 'D:/3D/Guild Wars 2/Extract/Strings/English'
foreign_folder = 'D:/3D/Guild Wars 2/Extract/Strings/German'

native_folder = os.path.abspath(native_folder)
foreign_folder = os.path.abspath(foreign_folder)

native_files = os.listdir(native_folder)
foreign_files = os.listdir(foreign_folder)
file_count = min(len(native_files), len(foreign_files))

def get_phrases_from_file(filepath: str) -> Dict[int, str]:
    phrases = {}
    with open(filepath, newline='', encoding='utf-8') as csvfile:
        reader = csv.reader(csvfile, delimiter=';', quotechar='"')
        for i, row in enumerate(reader):
            if not row:
                continue
            if len(row) == 1:
                # Sometimes there's no ID for some reason, so just put a unique negative number, 
                # since they never use negative numbers and we still want to identify them to 
                # make sure they match between the two files.
                row = (-i, row[0])
            phrases[row[0]] = row[1]
    return phrases

phrase_map = []

for i in range(file_count):
    native_file = native_files[i]
    foreign_file = foreign_files[i]

    native_phrases = get_phrases_from_file(os.path.join(native_folder, native_file))
    foreign_phrases = get_phrases_from_file(os.path.join(foreign_folder, foreign_file))

    for ID, native_string in native_phrases.items():
        if ID not in foreign_phrases:
            # print("Skipped", string)
            continue
        foreign_string = foreign_phrases[ID]
        phrase_map.append((native_string, foreign_string))
        if "sind schon dabei" in foreign_string:
            print(foreign_string)

# for el in phrase_map:
#     print(el)