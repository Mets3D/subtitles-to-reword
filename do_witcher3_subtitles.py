# Turning Witcher 3 subtitles into .csv files usable in the ReWord android app for language learning:
# - Make sure the Witcher 3 is installed, duh.
# - Download W3 Strings Decoder: https://witcher-games.fandom.com/wiki/W3strings
# - Input "root_folder", "WITCHER3_INSTALL_FOLDER", "W3STRINGS_DECODER_EXE" parameters below.
# - Execute this program with PHASE = 0 to decode all subtitles in the game.

# - Execute this program with PHASE = 1.
# - Execute Phase 2 manually:
    # - Copy the GT_foreign_folder folder
    # - Name the copy "GT_native_folder"
    # - For each file:
    #   - Copy & Paste the contents of the file into Google Translate in a web browser
    #   - Copy & Paste the resulting translation back into the file, overwriting its previous contents.
# - Execute this program with PHASE = 3.
# - Load the resulting .csv file in Re-Word.
# - Enjoy learning some foreign words!

PHASE = 3

# Since a lot of files will be involved, keep all your shit in one place.
root_folder = "D:/Met-PC/Desktop/reWord/Witcher3"
WITCHER3_INSTALL_FOLDER = "D:/Games/Steam/steamapps/common/The Witcher 3/"
# Filepath of the w3strings decoder .exe.
W3STRINGS_DECODER_EXE = root_folder + "/w3strings_decoder/w3strings.exe"

# Languages to decode
native_lang = "en"
foreign_lang = "de"

#######################################################
############# OPTIONAL PARAMETERS #####################
#######################################################

# Output directory of Phase 0: Decoding Witcher 3 string files.
TARGET_DIR = root_folder + "/game_files_decode/"

# Output folder to put word lists for Phase 1.
GT_foreign_folder = root_folder + "/google_de/"
# Folder where you operate during Phase 2, copy-pasting text into and out of Google Translate.
GT_native_folder = root_folder + "/google_en/"

# Number of the most common words you want to learn
NUM_WORDS = 700
# Words to exclude
from subtitles_to_reword import known_words
KNOWN_WORDS = known_words.known_words

# Ultimate output file that can be loaded into ReWord.
final_csv_output_path = root_folder + "/output.csv"

#######################################################################
#######################################################################
#######################################################################

from subtitles_to_reword import google_translate
from subtitles_to_reword.utils import phraselist_to_word_data
from subtitles_to_reword import reword
from subtitles_to_reword.witcher3 import w3_copy_and_decode, w3_load_phrases_folder, w3_load_mapped_phrases

if PHASE == 0:
    w3_copy_and_decode(langs=[native_lang, foreign_lang])

if PHASE == 1:
    de_texts = w3_load_phrases_folder(
        folder = TARGET_DIR
        ,lang = foreign_lang
    )

    word_data = phraselist_to_word_data([e.text for e in list(de_texts.values())])#, sort='LEN')

    google_translate.GT_generate_word_lists(word_data, GT_foreign_folder, KNOWN_WORDS)

if PHASE == 3:
    phrases = w3_load_mapped_phrases(TARGET_DIR)

    reword.phrases_to_reword(
        phrases
        ,GT_foreign_folder = GT_foreign_folder
        ,GT_native_folder = GT_native_folder
        ,output_filepath = final_csv_output_path
        ,start_count = 0
        ,end_count = NUM_WORDS
    )
