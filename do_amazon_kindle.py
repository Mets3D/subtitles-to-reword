"""
How to turn books purchased on Amazon into .txt files that can be processed by this program,
to be turned into .csv files which can be used in the ReWord android app for language learning:

PHASE 0: ACQUIRING THE BOOK IN .TXT FORMAT
- Buy the book on Amazon
- Download a version of the Kindle software of version 1.26 or lower.
    - You do this by googling for eg., "KindleForPC-installer-1.17", and then hope it's not a virus.
    - I re-hosted what I found on Dropbox, idk if it has malware: https://www.dropbox.com/s/0basem834nu5xt4/KindleForPC-installer-1.17.44183.exe?dl=0
    - If you already downloaded your book with a higher version, you should delete it.
- Download the book in the above Kindle software (lands in Users/Documents/My Kindle Content/crypticshit.azw)

- Download Calibre: https://calibre-ebook.com/
- Download DeDRM plugin: https://github.com/apprenticeharper/DeDRM_tools/releases
    - This will download a zip which contains 2 zips. You unzip only what you download.
    - Then open Calibre->Press Ctrl+P->Plugins->Load plug-in from File->Browse DeDRM_plugin.zip
    - Restart Calibre

- Drag&Drop your .azw file from Users/Documents/My Kindle Content, into Calibre.
- For me, using "Convert Books" button doesn't work at all (it says it output something to my Temp folder but it did not). 
- Instead, Edit book->File->Save As a Copy. This seems to save as .epub.
- Convert the .epub to .txt with some generic online tool or whatever.

PHASE 1: Create foreign word-list
 - Set up "root_folder" and "foreign_book_txt" parameters below.

 - Execute this program with PHASE = 1.

PHASE 2: Translate word-list with Google Translate
 - Copy the "GT_foreign_folder" folder
 - Name the copy "GT_native_folder"
 - For each file:
   - Copy & Paste the contents of the file into Google Translate in a web browser
   - Copy & Paste the resulting translation back into the file, overwriting its previous contents.

PHASE 3: Convert word-list to final .csv
 - Execute this program with PHASE = 3.
 - Load the resulting .csv file in Re-Word.
 - Enjoy learning some foreign words!
"""

PHASE = 3

root_folder = "D:/Met-PC/Desktop/reWord/Calibre/Momo"

# Folders where you downloaded the foreign and native language VTT files from Netflix
# (You can do this with a browser extension, consider this Phase 0)
foreign_book_txt = root_folder + "/Momo_German.txt"
# (Sadly, the example sentences cannot be matched with their native version, because there's no reliable way to know which sentence corresponds to which. Sometimes in the translation, two sentences are expressed with one, or vice versa, causing things to fall out of sync.)

#######################################################
############# OPTIONAL PARAMETERS #####################
#######################################################

# Number of the most common words you want to learn
NUM_WORDS = 1000
# Words to exclude
from subtitles_to_reword import known_words
KNOWN_WORDS = known_words.known_words

# Word index limits, for when you don't want too many words.
start = 0
end = 315

# Folder where a list of words will be outputted by running this program with PHASE==1.
GT_foreign_folder = root_folder + "/google_de/"
# Folder where you operate during Phase 2, copy-pasting text into and out of Google Translate.
GT_native_folder = root_folder + "/google_en/"

# Ultimate output file that can be loaded into ReWord.
final_csv_output_path = root_folder + "/output.csv"

#######################################################################
#######################################################################
#######################################################################

from subtitles_to_reword.utils import phraselist_to_word_data
from subtitles_to_reword import google_translate
from subtitles_to_reword import reword
import codecs

# Load the .txt file, separate strings per newline.
lines = []
f = codecs.open(foreign_book_txt, 'r', 'utf-8')
for line in f:
    if line == "":
        continue
    lines.append(line)
f.close()

word_data = phraselist_to_word_data(lines)

if PHASE == 1:
    google_translate.GT_generate_word_lists(word_data, GT_foreign_folder, KNOWN_WORDS)

if PHASE == 3:
    # Split up paragraphs to sentences.
    phrases = []
    for line in lines:
        line = line.replace("«", "").replace("»", "")
        sentences = line.split(".")
        for sentence in sentences:
            if len(sentence) > 15:
                phrases.append(sentence+".")

    # Again, since the english translation is not available here, we just use the foreign
    # phrase in both the foreign and native phrase slots, which is sad.
    phrases = {phrase : phrase for i, phrase in enumerate(phrases)}

    reword.phrases_to_reword(
        phrases
        ,GT_foreign_folder = GT_foreign_folder
        ,GT_native_folder = GT_native_folder
        ,output_filepath = final_csv_output_path
        ,start_count = start
        ,end_count = end
    )
